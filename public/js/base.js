$(document).ready(function () {

	var int_frames = 325;
	var path_images = "images/dani/V1-0001_dani-##.jpg"

	var myLenticular = new Lenticular.Image($("#myLenticular")[0], {
		images: path_images,
		frames: int_frames,
		useTilt: false,
		isActive: true,
		useTilt: true,
	});
	myLenticular.showFrame(0);
	myLenticular.activate();
	$(".lenticular-set").css("overflow", "");

	function onDeviceMotion(event) {

		var tiltAngle = event.accelerationIncludingGravity.z * 180 / Math.PI;
		var frame = Math.round(tiltAngle / 10);
		var progress = frame / int_frames;
		var smoothedFrame = Math.floor(progress * 10) + (1 - progress) * frame;
		smoothedFrame = Math.max(Math.min(smoothedFrame, int_frames - 1), 0);
		myLenticular.showFrame(smoothedFrame);
	}

	window.addEventListener("devicemotion", onDeviceMotion);

	$("#myLenticular img").mousemove(function (event) {
		var divWidth = $(this).width();
		var mouseX = event.pageX - $(this).offset().left;
		var percentage = mouseX / divWidth;
		var frame = Math.round(percentage * int_frames);
		// Ensure frame is within the range of 0 to int_frames-1
		frame = Math.max(Math.min(frame, int_frames - 1), 0);

		myLenticular.showFrame(frame);
	});
});
